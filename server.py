import socket
from ctypes import*
import time

 

localIP     = "127.0.0.1"
localPort   = 20001
bufferSize  = 1024

msgFromServer       = "Sent from python"

 
class Payload(Structure):
    _fields_ = [("f1", c_float),("f2",c_float), 
            ("f3", c_float), ("f4", c_float), 
            ("f5", c_float), ("f6", c_float)]
    def __repr__(self):
        print('f1: ', self.f1)

# Create a datagram socket
UDPServerSocket = socket.socket(family=socket.AF_INET, type=socket.SOCK_DGRAM)

# Bind to address and ip
#UDPServerSocket.bind((localIP, localPort))
 

#print("UDP server up listening")
print("Python UDP server up")


msg_to_send = Payload(0.1, 0.2, 0.3, 0.4, 0.5, 0.6);
# Listen for incoming datagrams
loop_cnt = 0
while(True):
    #bytesAddressPair = UDPServerSocket.recvfrom(bufferSize)
    #message = bytesAddressPair[0]
    #address = bytesAddressPair[1]
    #clientMsg = "Message from Client:{}".format(message)
    #clientIP  = "Client IP Address:{}".format(address)
    #print(clientMsg)
    #print(clientIP)
    msg_to_send.f1 = loop_cnt + 0.1
    msg_to_send.f2 = loop_cnt + 0.2
    msg_to_send.f3 = loop_cnt + 0.3
    msg_to_send.f4 = loop_cnt + 0.4
    msg_to_send.f5 = loop_cnt + 0.5
    msg_to_send.f6 = loop_cnt + 0.6
    #print(msg_to_send)
    
    print('Sent from python: ', loop_cnt)
    bytesToSend         = msg_to_send; #str.encode(msgFromServer+' ' + str(loop_cnt))

    # Sending a reply to client
    UDPServerSocket.sendto(bytesToSend, (localIP, localPort))

    loop_cnt=loop_cnt+1
    loop_cnt=loop_cnt%100
    time.sleep(1)
