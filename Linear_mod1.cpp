
#include "Linear.h"
#include <onnxruntime_cxx_api.h>
#include <array>
#include <iostream>
#include <iterator>
#include <algorithm>
#include"cnpy.h"
#include<complex>
#include<cstdlib>
#include<map>
#include<string>

#ifdef _WIN32
  const wchar_t* model_path = L"rally_lstm.onnx"; //LR"(linear.onnx)"
#else
  const char* model_path = "rally_lstm.onnx";
#endif

using namespace std;


void Demo::RunLinearRegression()
{
	// gives access to the underlying API (you can optionally customize log)
	// you can create one environment per process (each environment manages an internal thread pool)
	Ort::Env env;

	// creates an inference session for a certain model
	//Ort::Session session{ env, model_path, Ort::SessionOptions{} };
	// Ort::SessionOptions session_options(nullptr);
	Ort::Session session{env,model_path, Ort::SessionOptions{} };

	// Ort::Session gives access to input and output information:
	// - count
	// - name
	// - shape and type
	std::cout << "Number of model inputs: " << session.GetInputCount() << "\n";
	std::cout << "Number of model outputs: " << session.GetOutputCount() << "\n";

	// you can customize how allocation works. Let's just use a default allocator provided by the library
	Ort::AllocatorWithDefaultOptions allocator;
	// get input and output names
	auto* inputName = session.GetInputName(0, allocator);
	std::cout << "Input name: " << inputName << "\n";

	auto* outputName = session.GetOutputName(0, allocator);
	std::cout << "Output name: " << outputName << "\n";

	// get input shape
	auto inputShape = session.GetInputTypeInfo(0).GetTensorTypeAndShapeInfo().GetShape();
	//std::cout << session.GetInputTypeInfo(0).GetTensorTypeAndShapeInfo().GetShape()<<endl;
	
	// set some input values
	std::vector<float> inputValues = {1, 3, 4, 5, 6 , 6};
	for(int i=0;i<5;i++)
	{
		cout<<inputValues[i]<<" ";
	}
	/*cout<<inputValues.data()<<endl;
	cout<<inputValues.size()<<endl;
	cout<<inputShape.data()<<endl;
	cout<<inputShape.size()<<endl;*/
	//cnpy::NpyArray arr = cnpy::npy_load("np_data_3.npy");
	//float* loaded_data = arr.data<float>();
	//size_t nrows = arr.shape[1];
      	//size_t ncols = arr.shape[0];
   	//std::vector<float> inputValues;
	//std::cout<< ncols<<endl;	
   	//inputValues.reserve(nrows);
   	//for(size_t row = 0; row < nrows;row++) {
   	//inputValues.push_back(loaded_data[row]);
   	
       //inputValues.emplace_back(nrows);
      	//for(size_t col = 0;col < nrows;col++) 
      	//{
        //inputValues[row] = loaded_data[row];
        std::cout << *inputShape.data()<<endl;//inputValues[row] <<" ";
      	//}
  	 //}
	
	
	// where to allocate the tensors
	auto memoryInfo = Ort::MemoryInfo::CreateCpu(OrtDeviceAllocator, OrtMemTypeCPU);

	// create the input tensor (this is not a deep copy!)
	auto inputOnnxTensor = Ort::Value::CreateTensor<float>(memoryInfo, 
		inputValues.data(), inputValues.size(), 
		inputShape.data(), inputShape.size());
		
	// the API needs the array of inputs you set and the array of outputs you get
	array inputNames = { inputName };
	array outputNames = { outputName };

	// finally run the inference!
	auto outputValues = session.Run(
		Ort::RunOptions{ nullptr }, // e.g. set a verbosity level only for this run
		inputNames.data(), &inputOnnxTensor, 1, // input to set
		outputNames.data(), 1);					// output to take 

	// extract first (and only) output
	auto& output1 = outputValues[0];
	const auto* floats = output1.GetTensorMutableData<float>();
	const auto floatsCount = output1.GetTensorTypeAndShapeInfo().GetElementCount();
	std::cout<< floatsCount<< endl;
	// just print the output values
	std::copy_n(floats, floatsCount, ostream_iterator<float>(cout, " "));


	// closing boilerplate
	allocator.Free(inputName);
	allocator.Free(outputName);
}



using namespace std;

int main()
{
	try
	{
		Demo::RunLinearRegression();
	}
	catch (const exception& e)
	{
		cout << e.what() << "\n";

	}
}	
