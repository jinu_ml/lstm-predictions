import socket
from ctypes import*
import time
 

msgFromClient       = "Hello UDP Server"
bytesToSend         = str.encode(msgFromClient)
serverAddressPort   = ("127.0.0.1", 20001)
bufferSize          = 1024

class Payload(Structure):
    _fields_ = [("f1", c_float),("f2",c_float),
            ("f3", c_float), ("f4", c_float),
            ("f5", c_float), ("f6", c_float)]

 

# Create a UDP socket at client side
UDPClientSocket = socket.socket(family=socket.AF_INET, type=socket.SOCK_DGRAM)

# Send to server using created UDP socket
#UDPClientSocket.sendto(bytesToSend, serverAddressPort)

UDPClientSocket.bind(serverAddressPort)
msg_to_recv = Payload(0.1, 0.2, 0.3, 0.4, 0.5, 0.6);
while True:
    msgFromServer = UDPClientSocket.recvfrom(bufferSize)
    msg_to_recv = Payload.from_buffer_copy(msgFromServer[0])

    print("In Python. Message from some Server.")
    print(msg_to_recv.f1, end='\t')
    print(msg_to_recv.f2, end='\t')
    print(msg_to_recv.f3, end='\t')
    print(msg_to_recv.f4, end='\t')
    print(msg_to_recv.f5, end='\t')
    print(msg_to_recv.f6)
