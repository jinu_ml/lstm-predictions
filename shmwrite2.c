#include <sys/ipc.h>
#include <sys/shm.h>
#include <unistd.h>
#include <stdio.h>
#include<stdlib.h>  
#include<string.h>
#define GETEKYDIR ("/tmp")
struct data
{
	float f1;
	float f2;
	float f3;
	float f4;
	float f5;
	float f6;
        float f7;
};
typedef struct data node;
int main()
{
    
    // ftok to generate unique key
    key_t key = 103;
  
    // shmget returns an identifier in shmid
    int shmid = shmget(key,1024,0666|IPC_CREAT);
  
    // shmat to attach to shared memory
    node *d = (node*) shmat(shmid,(void*)0,0);
    int loop_cnt = 0;
    //d->f7=1;
    while(1)
     {	    
    	d->f1=loop_cnt+0.1;
    	d->f2=loop_cnt+0.2;
    	d->f3=loop_cnt+0.3;
    	d->f4=loop_cnt+0.4;
    	d->f5=loop_cnt+0.5;
    	d->f6=loop_cnt+0.6;
        

    printf("Data written in memory: %d\n",loop_cnt);
    printf("roll cmd: %f\n",d->f7);
    loop_cnt = loop_cnt+1;
    sleep(1);  

     }	
    //detach from shared memory 
    shmdt(d);
  
    return 0;
}
