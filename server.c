// Server side implementation of UDP client-server model
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <netinet/in.h>
  
//#define PORT     20001
#define MAXLINE 1024


struct FLTSTR
{
	float f1;
	float f2;
	float f3;
	float f4;
	float f5;
	float f6;
}FLT;

//struct FLTSTR FLT;
int PORT; 

// Driver code
int main(int argc, char **argv) {
    int sockfd;
    char buffer[MAXLINE];
    struct sockaddr_in addr;
	PORT = atoi(argv[1]);  
    // Creating socket file descriptor
    if ( (sockfd = socket(AF_INET, SOCK_DGRAM, 0)) < 0 ) {
        perror("socket creation failed");
        exit(EXIT_FAILURE);
    }
  
    memset(&addr, 0, sizeof(addr));
      
    // Filling server information
    addr.sin_family = AF_INET;
    addr.sin_port = htons(PORT);
    addr.sin_addr.s_addr = INADDR_ANY;
  
int loop_cnt = 0;
while(1)  
{
	FLT.f1=loop_cnt + 0.1;
	FLT.f2=loop_cnt + 0.2;
	FLT.f3=loop_cnt + 0.3;
	FLT.f4=loop_cnt + 0.4;
	FLT.f5=loop_cnt + 0.5;
	FLT.f6=loop_cnt + 0.6;
	//char hello = FLT; 
    sendto(sockfd, (const char*)&FLT, sizeof(FLT),
        MSG_CONFIRM, (const struct sockaddr *) &addr, 
            sizeof(addr));
    printf("Sent FLT with %d\n",loop_cnt);
    loop_cnt = loop_cnt+1;
    if(loop_cnt>100) loop_cnt = 0;
    sleep(1);
} 
    close(sockfd);
    return 0;
}
