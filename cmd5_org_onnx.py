import socket
import struct
import onnxruntime
import sys
from ctypes import*
import numpy as np
import time
 
class Payload(Structure):
    _fields_ = [("f1", c_float),("f2",c_float), ("f3", c_float), ("f4", c_float), ("f5", c_float), ("f6", c_float)]
UDP_IP = "127.0.0.1"
UDP_PORT = 20001




session = onnxruntime.InferenceSession("model4.onnx")

serverAddressPort0=(UDP_IP,UDP_PORT)
serverAddressPort1=(UDP_IP,UDP_PORT-1)

sock = socket.socket(socket.AF_INET,socket.SOCK_DGRAM) # UDP
sock.bind(serverAddressPort1)
while True:
	d1=[]
	d2=[]
	d3=[]
	d4=[]
	d5=[]
	d6=[]
	buff= sock.recvfrom(sizeof(Payload))
	payload_in=Payload.from_buffer_copy(buff[0])
	print("float {}".format(payload_in.f1))
	print("float {}".format(payload_in.f2))
	print("float {}".format(payload_in.f3))
	print("float {}".format(payload_in.f4))
	print("float {}".format(payload_in.f5))
	print("float {}".format(payload_in.f6))

	d1.append(payload_in.f1)
	d2.append(payload_in.f2)
	d3.append(payload_in.f3)
	d4.append(payload_in.f4)
	d5.append(payload_in.f5)
	d6.append(payload_in.f6)

	data = list(zip(d1 ,d2 ,d3, d4, d5, d6 ))
	data = np.asarray(data,dtype=np.float32)
	print(data)
	rows = data.shape[0]
	cols =  data.shape[1]
	test_X= data.reshape(rows,1,cols)
	print('***'*60)
	print(test_X)
	print(test_X.shape)
	#input_name = session.get_inputs()[0].name
	test_X.shape
	prediction = session.run(None, {"lstm_2_input": test_X})
	yhat = np.asarray(prediction,  dtype=np.float32)
	yhat = yhat.reshape(1)
	
	y_max= 17.0079
	y_min= -21.7323
	y_mean= -0.3836
	
	y_org = (yhat*(y_max-y_min))+y_mean 
	print('output_type {}'.format(type(y_org)))
	print('output_shape {}'.format(y_org.shape))
	print('output_cmd {}'.format(y_org))
	fy_org = c_float(y_org)
	#fyhat=sock.htonl(Int32bit_postive)
	print('sent_data_type {}'.format(type(fy_org)))
	print('sent_data {}'.format(fy_org))
	sock.sendto(fy_org,serverAddressPort0)
	print("Data sent sucessfully\n")

