// Client side implementation of UDP client-server model
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <netinet/in.h>
  
//#define PORT     20001
#define MAXLINE 1024
struct
{       
        float f1;
        float f2;
        float f3;
        float f4;
        float f5;
        float f6;
}FLT;
int PORT;

// Driver code
int main(int argc, char **argv) {

    int sockfd;
    //char buffer[MAXLINE];
    struct sockaddr_in servaddr;
        PORT = atoi(argv[1]);
      
    // Creating socket file descriptor
    if ( (sockfd = socket(AF_INET, SOCK_DGRAM, 0)) < 0 ) {
        perror("socket creation failed");
        exit(EXIT_FAILURE);
    }
      
    memset(&servaddr, 0, sizeof(servaddr));
      
    // Filling server information
    servaddr.sin_family    = AF_INET; // IPv4
    servaddr.sin_addr.s_addr = INADDR_ANY;
    servaddr.sin_port = htons(PORT);
      
    // Bind the socket with the server address
    if ( bind(sockfd, (const struct sockaddr *)&servaddr, 
            sizeof(servaddr)) < 0 )
    {
        perror("bind failed");
        exit(EXIT_FAILURE);
    }
      
    int len, n;
 
   while(1) 
   {
    len = sizeof(servaddr);  //len is value/resuslt
  
    n = recvfrom(sockfd, (char *)&FLT, MAXLINE, 
                MSG_WAITALL, ( struct sockaddr *) &servaddr,
                &len);
    printf("in C, Received from some Server : %f\n", 
		    	FLT.f1);
   }  
    return 0;
}
