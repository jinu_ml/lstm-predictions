#python 

# Install sysv_ipc module firstly if you don't have this
import sysv_ipc as ipc
import struct
import sys
import onnxruntime
from ctypes import*
import numpy as np
import utils

def main():
    class Payload(Structure):
   # 	_fields_ = [("f1",c_float),("f2",c_float),("f3",c_float),("f4",c_float),("f5",c_float),("f6",c_float),("f7",c_float)]
       _fields_ = [("f1",c_float),("f2",c_float),("f3",c_float),("f4",c_float),("f5",c_float),("f6",c_float)]

    	
    path = "/tmp"
    key1 = 5670
    shm = ipc.SharedMemory(key1, 0, 0)
    session = onnxruntime.InferenceSession("model4.onnx")
    #I found if we do not attach ourselves
    #it will attach as ReadOnly.
    shm.attach(0,0)  
    while True:
       d1=[]
       d2=[]
       d3=[]
       d4=[]
       d5=[]
       d6=[]
       #d7=[]
       buff = shm.read(sizeof(Payload))
       payload_in=Payload.from_buffer_copy(buff)
       print("float {}".format(payload_in.f1))
       print("float {}".format(payload_in.f2))
       print("float {}".format(payload_in.f3))
       print("float {}".format(payload_in.f4))
       print("float {}".format(payload_in.f5))
       print("float {}".format(payload_in.f6))
       #print("float {}".format(payload_in.f7))

  
       d1.append(payload_in.f1)
       d2.append(payload_in.f2)
       d3.append(payload_in.f3)
       d4.append(payload_in.f4)
       d5.append(payload_in.f5)
       d6.append(payload_in.f6)

       data = list(zip(d1 ,d2 ,d3, d4, d5, d6 ))
       data = np.asarray(data,dtype=np.float32)
       print(data)
       print('###'*10)
       print(data[0][1])

       u1=0.1857
       u2=-1.7299
       u3=0.2125
       u4=0.2125
       u5=-0.4247
       u6=-0.3836
	
       d1_min=0
       d2_min=-20.3116
       d3_min=0
       d4_min=-1.0018
       d5_min=-1.0293
       d6_min=-21.7323

       d1_max=1.3877
       d2_max=21.3308
       d3_max=1
       d4_max=3.3141
       d5_max=0.0508
       d6_max=17.0079
			

       rows = data.shape[0]
       cols =  data.shape[1]
       #test_X= data.reshape(rows,1,cols)
       test_X= data.reshape(1,1,cols)

       print('***'*60)
       print(test_X)
       print(test_X.shape)
       #input_name = session.get_inputs()[0].name
       test_X.shape
       prediction = session.run(None, {"lstm_2_input": test_X})
       yhat = np.asarray(prediction,  dtype=np.float32)
       yhat = yhat.reshape(1)

       y_max= 17.0079
       y_min= -21.7323
       y_mean= -0.3836

       y_org = (yhat*(y_max-y_min))+y_mean 
       print('output_type {}'.format(type(y_org)))
       print('output_shape {}'.format(y_org.shape))
       print('output_cmd {}'.format(y_org))

       #shm.detach()
       key2 = 5680
       mem = ipc.SharedMemory(key2,0,0)
       mem.attach(0,0)
       fy_org = c_float(y_org)
       #fyhat=sock.htonl(Int32bit_postive)
       #Payload.f7 = fy_org 
       #utils.write_to_memory(mem,Payload.f7)
       mem.write(fy_org,0) 
       #print('shrm data-{}'.format(Payload.f7))   
       print('sent_data_type {}'.format(type(fy_org)))
       print('sent_data {}'.format(fy_org))

#shm.detach()
#pass
	#shm.detach()
if __name__ == '__main__':
    main()
